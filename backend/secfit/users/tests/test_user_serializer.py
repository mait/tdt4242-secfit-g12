
from unittest import expectedFailure
from django.forms import model_to_dict
from django.test import TestCase
from users.serializers import UserSerializer
from django.contrib.auth import get_user_model
from rest_framework.test import APIRequestFactory
from rest_framework.request import Request

class UserSerializerTests(TestCase):
    def test_validate_password(self):
        EXPECTED_PASSWORD = "test123"
        data = {
            "username": "user",
            "password": EXPECTED_PASSWORD
        }
        
        serializer = UserSerializer(data=data)
        password = serializer.validate_password(EXPECTED_PASSWORD)
        
        self.assertEqual(EXPECTED_PASSWORD, password)
        
    
    def test_create_user(self):
        username = "User"
        email = "user@secfit.no"
        password = "password123"
        phone_number = "46948562"
        country = "Norway"
        city = "Trondheim"
        street_address = "Dragvoll"
        
        data = {
            "username": username,
            "email": email,
            "password": password,
            "phone_number": phone_number,
            "country": country,
            "city": city,
            "street_address": street_address,
        }
        
        serializer = UserSerializer()
        
        # Test that no users created yet
        self.assertFalse(get_user_model().objects.exists())
        user = serializer.create(data)
        
        # Test that a user has been created
        self.assertTrue(get_user_model().objects.exists())
        
        FIRST_USER_ID = 1
        self.assertEqual(FIRST_USER_ID, user.pk)
        
        self.assertEqual(username, user.username)
        
