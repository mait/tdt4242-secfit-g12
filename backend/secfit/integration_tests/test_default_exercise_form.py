import random
import string
from django.test import LiveServerTestCase
from selenium import webdriver
import time
from django.test import tag
from selenium.webdriver.common.keys import Keys

TIME_WAIT = 1

def get_random_string(length=random.randint(3, 20)):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for _ in range(length))

@tag("integration")
class ExerciseFormBoundaryTests(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = webdriver.Chrome("../../chromedriver.exe")
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
    
    url_login = "http://localhost:8080/login.html"
    url_exercises = "http://localhost:8080/exercises.html"
    url_exercise = "http://localhost:8080/exercise.html"
    
    def login_and_enter_exercise_form(self):
        self.selenium.get(self.url_login)
        
        self.fill_input_field("username", "admin")
        self.fill_input_field("password", "123")
        self.selenium.find_element_by_xpath("//*[@id='btn-login']").click()
        time.sleep(TIME_WAIT)
        
        self.selenium.get(self.url_exercises)
        
        self.selenium.find_element_by_xpath("//*[@id='btn-create-exercise']").click()
        time.sleep(TIME_WAIT)
        
    def fill_input_field(self, inputName, inputValue):
        field_input = self.selenium.find_element_by_name(inputName)
        field_input.send_keys(inputValue)
        
    def fill_duration_input_field(self, value):
        self.fill_input_field("name", "Exercise")
        self.fill_input_field("description", "Description")
        self.fill_input_field("unit", "seconds")
        self.fill_input_field("duration", value)
        self.fill_input_field("calories", 120)
        
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_xpath("//*[@id='btn-ok-exercise']").click()
        time.sleep(TIME_WAIT)
        
    def fill_calories_burned_input_field(self, value):
        self.fill_input_field("name", "Exercise")
        self.fill_input_field("description", "Description")
        self.fill_input_field("unit", "seconds")
        self.fill_input_field("duration", 360)
        self.fill_input_field("calories", value)
        
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_xpath("//*[@id='btn-ok-exercise']").click()
        time.sleep(TIME_WAIT)
        
    def fill_unit_input_field(self, value):
        self.fill_input_field("name", "Exercise")
        self.fill_input_field("description", "Description")
        self.fill_input_field("unit", value)
        self.fill_input_field("duration", 360)
        self.fill_input_field("calories", 120)
        
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_xpath("//*[@id='btn-ok-exercise']").click()
        time.sleep(TIME_WAIT)
        
    def assert_succesful_submission(self):
        expected_url = self.url_exercises
        redirected_url = self.selenium.current_url
        self.assertEqual(redirected_url, expected_url, "User was not redirected to page for all exercises")
        time.sleep(TIME_WAIT)
    
    def assert_invalid_field_name(self, field_name):
        msg = self.selenium.find_element_by_xpath("/html/body/div[1]").text.lower()
        self.assertIn("could not create", msg) 
        self.assertIn(field_name, msg)
        time.sleep(TIME_WAIT)
    
    def assert_failed_submission(self):
        expected_url = self.url_exercise
        redirected_url = self.selenium.current_url
        self.assertEqual(redirected_url, expected_url, "User was redirected to another page")
        time.sleep(TIME_WAIT)
    
    def test_duration_negative_value(self):
        self.login_and_enter_exercise_form()
        self.fill_duration_input_field(-50)
        
        # the implementation won't make this test fail
        self.assert_succesful_submission()
    
    def test_duration_negative_threshold_value(self):
        self.login_and_enter_exercise_form()
        self.fill_duration_input_field(-1)
        
        # the implementation won't make this test fail
        self.assert_succesful_submission()
    
    def test_duration_zero_value(self):
        self.login_and_enter_exercise_form()
        self.fill_duration_input_field(0)
        
        # the implementation won't make this test fail
        self.assert_succesful_submission()
    
    def test_duration_blank_value(self):
        self.login_and_enter_exercise_form()
        self.fill_duration_input_field("")
        
        self.assert_failed_submission()
        
        self.assert_invalid_field_name("duration")
        
    def test_duration_positive_threshold_value(self):
        self.login_and_enter_exercise_form()
        self.fill_duration_input_field(1)
        
        self.assert_succesful_submission()
        
    def test_duration_positive_value(self):
        self.login_and_enter_exercise_form()
        self.fill_duration_input_field(360)
        
        self.assert_succesful_submission()
        
    def test_calories_burned_negative_value(self):
        self.login_and_enter_exercise_form()
        self.fill_calories_burned_input_field(-356)
        
        # the implementation won't make this test fail
        self.assert_succesful_submission()
    
    def test_calories_burned_negative_threshold_value(self):
        self.login_and_enter_exercise_form()
        self.fill_calories_burned_input_field(-1)
        
        # the implementation won't make this test fail
        self.assert_succesful_submission()
    
    def test_calories_burned_negative_zero_value(self):
        self.login_and_enter_exercise_form()
        self.fill_calories_burned_input_field(0)
        
        # the implementation won't make this test fail
        self.assert_succesful_submission()
    
    def test_calories_burned_negative_blank_value(self):
        self.login_and_enter_exercise_form()
        self.fill_calories_burned_input_field("")
        
        self.assert_failed_submission()
        
        self.assert_invalid_field_name("calories")
    
    def test_calories_burned_positive_threshold_value(self):
        self.login_and_enter_exercise_form()
        self.fill_calories_burned_input_field(1)
        
        self.assert_succesful_submission()
    
    def test_calories_burned_positive_value(self):
        self.login_and_enter_exercise_form()
        self.fill_calories_burned_input_field(1200)
        
        self.assert_succesful_submission()
    
    def test_unit_length_1(self):
        self.login_and_enter_exercise_form()
        unit = "s" 
        self.assertEqual(len(unit), 1)
        self.fill_unit_input_field(unit)
        
        # this should have failed
        self.assert_succesful_submission()
    
    def test_unit_blank_value(self):
        self.login_and_enter_exercise_form()
        unit = ""
        self.assertEqual(len(unit), 0)
        self.fill_unit_input_field(unit)
        
        self.assert_failed_submission()
        
        self.assert_invalid_field_name("unit")
    
    def test_unit_length_7(self):
        self.login_and_enter_exercise_form()
        unit = "seconds" 
        self.assertEqual(len(unit), 7)
        self.fill_unit_input_field(unit)
        
        self.assert_succesful_submission() 
    
    def test_unit_length_50(self):
        self.login_and_enter_exercise_form()
        BOUNDARY = 50
        unit = "a"*BOUNDARY
        self.assertEqual(len(unit), BOUNDARY)
        self.fill_unit_input_field(unit)
        
        # this should have failed
        self.assert_succesful_submission() 
    
    def test_unit_special_symbols(self):
        self.login_and_enter_exercise_form()
        unit = "@ASDAS[]^~ÆS*"
        self.fill_unit_input_field(unit)
        
        # this should have failed
        self.assert_succesful_submission() 
