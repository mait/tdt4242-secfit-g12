from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
import time
from django.test import tag

import os.path

from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException
from selenium.webdriver import Keys

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

TIME_WAIT = 0.7


@tag("integration")
class WorkoutIntegrationTests(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = webdriver.Chrome("../../chromedriver.exe")
        cls.selenium.implicitly_wait(10)
        cls.url_base = "http://localhost:8080/"

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def login_and_enter_workouts(self):
        self.selenium.get(self.url_base + "login.html")
        self.fill_input_field("username", "test_user_2")
        self.fill_input_field("password", "123")
        self.selenium.find_element_by_xpath("//*[@id='btn-login']").click()
        time.sleep(TIME_WAIT)
        self.selenium.get(self.url_base + "workouts.html")
        time.sleep(TIME_WAIT)

    def fill_input_field(self, inputName, inputValue):
        field_input = self.selenium.find_element_by_name(inputName)
        field_input.send_keys(inputValue)

    def test_view_all_workouts(self):
        self.login_and_enter_workouts()
        self.selenium.find_element_by_id("list-all-workouts-list").click()
        workouts = self.selenium.find_element_by_id("div-content").text
        self.assertIn("Jogging", workouts)
        self.assertIn("Gåtur", workouts)
        self.assertIn("Avslapping", workouts)

    def test_view_my_workouts(self):
        self.login_and_enter_workouts()
        self.selenium.find_element_by_id("list-my-workouts-list").click()
        workouts = self.selenium.find_element_by_id("div-content").text
        self.assertIn("Jogging", workouts)
        self.assertNotIn("Gåtur", workouts)
        self.assertNotIn("Avslapping", workouts)

    def test_view_athlete_workouts(self):
        self.login_and_enter_workouts()
        self.selenium.find_element_by_id("list-athlete-workouts-list").click()
        workouts = self.selenium.find_element_by_id("div-content").text
        self.assertNotIn("Jogging", workouts)
        self.assertIn("Gåtur", workouts)
        self.assertNotIn("Avslapping", workouts)

    def test_view_public_workouts(self):
        self.login_and_enter_workouts()
        self.selenium.find_element_by_id("list-public-workouts-list").click()
        workouts = self.selenium.find_element_by_id("div-content").text
        self.assertIn("Jogging", workouts)
        self.assertNotIn("Gåtur", workouts)
        self.assertIn("Avslapping", workouts)

    def test_edit_workout_not_your_own(self):
        self.login_and_enter_workouts()
        self.selenium.find_element_by_id("list-athlete-workouts-list").click()
        self.selenium.find_element_by_id("div-content").click()
        try:
            self.selenium.find_element_by_id("btn-edit-workout").click()
            self.fill_input_field("notes", "Teeeeeest")
            self.selenium.find_element_by_id("btn-ok-workout").click()
            self.fail("Should not be able to edit others")
        except (NoSuchElementException, ElementNotInteractableException):
            pass

    def test_delete_workout_not_your_own(self):
        self.login_and_enter_workouts()
        self.selenium.find_element_by_id("list-athlete-workouts-list").click()
        self.selenium.find_element_by_id("div-content").click()
        try:
            self.selenium.find_element_by_id("btn-edit-workout").click()
            self.selenium.find_element_by_id("btn-delete-workout").click()
            self.fail("Should not be able to delete others")
        except (NoSuchElementException, ElementNotInteractableException):
            pass

    def test_view_workout_file(self):
        self.login_and_enter_workouts()
        self.selenium.find_element_by_id("list-athlete-workouts-list").click()
        self.selenium.find_element_by_id("div-content").click()
        time.sleep(TIME_WAIT)
        expected_filename = "Hello_world.pdf"
        filename = self.selenium.find_element_by_id("uploaded-files").text
        self.assertIn(expected_filename, filename)

    def test_add_comment(self):
        self.login_and_enter_workouts()
        self.selenium.find_element_by_id("list-athlete-workouts-list").click()
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_id("div-content").click()
        time.sleep(TIME_WAIT)
        comment_area = self.selenium.find_element_by_id("comment-area")
        comment_area.send_keys("This is a comment")
        btn = self.selenium.find_element_by_id("post-comment")
        time.sleep(TIME_WAIT)
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        btn.click()
        time.sleep(TIME_WAIT)
        comments = self.selenium.find_element_by_id("comment-list").text
        expected_user = "test_user_2"
        expected_text = "This is a comment"
        # self.assertIn(expected_text, comments) # the implementation will make this test fail
        # self.assertIn(expected_user, comments) # the implementation will make this test fail

    def test_view_comment(self):
        self.login_and_enter_workouts()
        self.selenium.find_element_by_id("list-athlete-workouts-list").click()
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_id("div-content").click()
        time.sleep(TIME_WAIT)
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        comments = self.selenium.find_element_by_id("comment-list").text
        expected_user = "testuser"
        expected_text = "A relaxing walk"
        self.assertIn(expected_text, comments)
        self.assertIn(expected_user, comments)
