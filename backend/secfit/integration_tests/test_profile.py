from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
import time
from django.test import tag

import os.path


PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

TIME_WAIT = 0.7

@tag("integration")
class ProfileIntegrationTests(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = webdriver.Chrome("../../chromedriver.exe")
        cls.selenium.implicitly_wait(10)
        cls.url_base = "http://localhost:8080/"

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def register_profile(self):
        self.selenium.get(self.url_base + "register.html")
        self.fill_input_field("username", "test_user")
        self.fill_input_field("email", "test@test.no")
        self.fill_input_field("password", "123")
        self.fill_input_field("password1", "123")
        self.fill_input_field("phone_number", "12345678")
        self.fill_input_field("country", "Norway")
        self.fill_input_field("city", "Trondheim")
        self.fill_input_field("street_address", "Høgskoleringen 1")
        self.selenium.find_element_by_xpath("//*[@id='btn-create-account']").click()

    def login_and_enter_profile(self):
        self.selenium.get(self.url_base + "login.html")

        self.fill_input_field("username", "test_user")
        self.fill_input_field("password", "123")
        self.selenium.find_element_by_xpath("//*[@id='btn-login']").click()
        time.sleep(TIME_WAIT)
        if self.selenium.current_url == self.url_base + "login.html":
            self.register_profile()
            self.selenium.get(self.url_base + "login.html")

            self.fill_input_field("username", "test_user")
            self.fill_input_field("password", "123")
            self.selenium.find_element_by_xpath("//*[@id='btn-login']").click()

        self.selenium.get(self.url_base + "profile.html")

        time.sleep(TIME_WAIT)
        time.sleep(TIME_WAIT)
        time.sleep(TIME_WAIT)

    def login_and_enter_second_profile(self):
        self.selenium.get(self.url_base + "login.html")

        self.fill_input_field("username", "test_user_2")
        self.fill_input_field("password", "123")
        self.selenium.find_element_by_xpath("//*[@id='btn-login']").click()
        time.sleep(TIME_WAIT)

        self.selenium.get(self.url_base + "profile.html")

        time.sleep(TIME_WAIT)

    def enter_settings(self):
        self.selenium.get(self.url_base + "settings.html")
        time.sleep(TIME_WAIT)
        time.sleep(TIME_WAIT)

    def fill_input_field(self, inputName, inputValue):
        field_input = self.selenium.find_element_by_name(inputName)
        field_input.send_keys(inputValue)

    def change_input_field(self, inputName, inputValue):
        field_input = self.selenium.find_element_by_name(inputName)
        field_input.clear()
        field_input.send_keys(inputValue)

    def assert_succesful_submission(self):
        expected_url = self.url_base + "profile.html"
        redirected_url = self.selenium.current_url
        self.assertEqual(expected_url, redirected_url, "User was not redirected to profile")
        time.sleep(TIME_WAIT)

    def assert_invalid_field_name(self, field_name):
        msg = self.selenium.find_element_by_xpath("/html/body/div[1]").text.lower()
        self.assertIn("could not create", msg)
        self.assertIn(field_name, msg)
        time.sleep(TIME_WAIT)

    def assert_failed_submission(self):
        expected_url = self.url_base + "settings.html"
        redirected_url = self.selenium.current_url
        self.assertEqual(redirected_url, expected_url, "User was redirected to another page")
        time.sleep(TIME_WAIT)

    def create_workout(self):
        self.selenium.get(self.url_base + "workouts.html")
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_id("btn-create-workout").click()
        time.sleep(TIME_WAIT)
        self.fill_input_field("name", "Avslapping")
        self.fill_input_field("date", "22.02.2022")
        datetime = self.selenium.find_element_by_name("date")
        datetime.send_keys(webdriver.Keys.TAB)
        datetime.send_keys("22:22")
        self.fill_input_field("notes", "Notes")
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_id("btn-ok-workout").click()
        time.sleep(TIME_WAIT)
        time.sleep(TIME_WAIT)
        time.sleep(TIME_WAIT)
        time.sleep(TIME_WAIT)
        self.selenium.get(self.url_base + "profile.html")
        time.sleep(TIME_WAIT)

    def test_basic_info(self):
        self.login_and_enter_profile()
        basic_info = self.selenium.find_element_by_id("basic-info").text
        self.assertIn("test@test.no", basic_info)
        self.assertIn("12345678", basic_info)
        self.assertIn("Norway", basic_info)
        self.assertIn("Trondheim", basic_info)
        self.assertIn("Høgskoleringen 1", basic_info)
        self.assertIn("No coach registered", basic_info)

    def test_username(self):
        self.login_and_enter_profile()
        username = self.selenium.find_element_by_id("profile-username").text
        self.assertEqual("test_user", username)

    def test_your_athletes_without_athlete(self):
        self.login_and_enter_profile()
        athletes = self.selenium.find_element_by_id("athletes-list").text
        self.assertIn("You have no athletes", athletes)

    def test_profile_workouts(self):
        self.login_and_enter_profile()
        workouts = self.selenium.find_element_by_id("workouts-list").text
        if workouts == "Workouts":
            self.create_workout()
            workouts = self.selenium.find_element_by_id("workouts-list").text

        self.assertIn("Avslapping", workouts)
        self.assertIn("test_user", workouts)

    def test_profile_workout_link(self):
        self.login_and_enter_profile()
        self.selenium.find_element_by_class_name("list-group-item").click()
        time.sleep(TIME_WAIT)
        expected_url = self.url_base + "workout.html"
        redirected_url = self.selenium.current_url
        self.assertIn(expected_url, redirected_url, "User was not redirected to workoutpage")

    def test_your_athletes_with_athlete(self):
        self.login_and_enter_second_profile()
        athletes = self.selenium.find_element_by_id("athletes-list").text
        self.assertIn("testuser", athletes)

    def test_coach(self):
        self.login_and_enter_second_profile()
        basic_info = self.selenium.find_element_by_id("basic-info").text.lower()
        self.assertIn("testuser", basic_info)

    def test_change_valid_email(self):
        self.login_and_enter_second_profile()
        self.enter_settings()
        self.change_input_field("email", "test@gmail.com")
        self.selenium.find_element_by_xpath("//*[@id='btn-save-changes']").click()
        time.sleep(TIME_WAIT)
        self.assert_succesful_submission()
        basic_info = self.selenium.find_element_by_id("basic-info").text
        self.assertIn("test@gmail.com", basic_info)

    def test_change_invalid_email(self):
        self.login_and_enter_second_profile()
        self.enter_settings()
        self.change_input_field("email", "test@@test.no")
        self.selenium.find_element_by_xpath("//*[@id='btn-save-changes']").click()
        self.assert_failed_submission()
