import random
import string
from django.test import LiveServerTestCase
from selenium import webdriver
import time
from django.test import tag
from selenium.webdriver.common.keys import Keys

TIME_WAIT = 1

def get_random_string(length=random.randint(3, 20)):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for _ in range(length))

@tag("integration")
class SignUpBoundaryTests(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = webdriver.Chrome("../../chromedriver.exe")
        cls.selenium.implicitly_wait(10)

    
    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
        
    url_login = "http://localhost:8080/register.html"
    url_workouts = "http://localhost:8080/workouts.html"
    
    def fill_input_field(self, inputName, inputValue):
        field_input = self.selenium.find_element_by_name(inputName)
        field_input.send_keys(inputValue)
        
    def fill_password(self):
        password = "password123"
        self.fill_input_field("password", password)
        self.fill_input_field("password1", password)
        time.sleep(TIME_WAIT)
        
    def fill_necessary_form(self):
        username = get_random_string()
        self.fill_input_field("username", username)
        self.fill_password()
    
    def assert_successful_registration(self):
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_xpath("//*[@id='btn-create-account']").click()
        time.sleep(TIME_WAIT)
        expected_url = self.url_workouts
        redirected_url = self.selenium.current_url
        error_msg = "User was not redirected to main page"
        self.assertEqual(redirected_url, expected_url, error_msg)
        time.sleep(TIME_WAIT)
        
    def assert_failed_registration(self):
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_id("btn-create-account").click()
        time.sleep(TIME_WAIT)
        
        expected_url = self.url_login
        redirected_url = self.selenium.current_url
        error_msg = "User was redirected to another page"
        self.assertEqual(redirected_url, expected_url, error_msg)
        time.sleep(TIME_WAIT)
    
    def assert_invalid_field_name(self, field_name):
        msg = self.selenium.find_element_by_xpath("/html/body/div[1]").text.lower()
        self.assertIn("failed", msg)
        self.assertIn(field_name, msg) 
        time.sleep(TIME_WAIT)
        
    def assert_invalid_username_field(self):
        self.assert_invalid_field_name("username")
        
    def assert_invalid_email_field(self):
        self.assert_invalid_field_name("email")
    
    def test_registration_with_all_valid_inputs(self):
        self.selenium.get(self.url_login)
        
        username = get_random_string()
        self.fill_input_field("username", username)
        self.fill_input_field("email", "user@secfit.no")
        self.fill_input_field("password", "")
        self.fill_input_field("password1", "")
        self.fill_input_field("phone_number", "12345678")
        self.fill_input_field("country", "Norway")
        self.fill_input_field("city", "Trondheim")
        self.fill_input_field("street_address", "Glos")
        
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_xpath("//*[@id='btn-create-account']").click()
        time.sleep(TIME_WAIT)
        msg = self.selenium.find_element_by_xpath("/html/body/div[1]").text
        self.assertIn("failed", msg)
        time.sleep(TIME_WAIT)
    
    def test_username_only_digits(self):
        self.selenium.get(self.url_login)
        username = str(random.randint(100, 1000000000))
        self.fill_input_field("username", username)
        self.fill_password()
        
        # test should have failed
        self.assert_successful_registration()
    
    def test_username_special_chars(self):
        self.selenium.get(self.url_login)
        username = f"[{get_random_string()}!@"
        self.fill_input_field("username", username)
        self.fill_password()
        
        self.assert_failed_registration()
        
        self.assert_invalid_username_field
    
    def test_username_blank(self):
        self.selenium.get(self.url_login)
        username = ""
        self.fill_input_field("username", username)
        self.fill_password()
        
        self.assert_failed_registration()
        
        self.assert_invalid_username_field()
    
    def test_username_boundary_chars(self):
        self.selenium.get(self.url_login)
        username = f"{get_random_string()}~"
        self.fill_input_field("username", username)
        self.fill_password()
        
        self.assert_failed_registration()
        
        self.assert_invalid_username_field()
    
    def test_username_extended_ascii_set(self):
        self.selenium.get(self.url_login)
        username = f"{get_random_string()}βÆØ"
        self.fill_input_field("username", username)
        self.fill_password()
        
        # the implementation won't make this test fail
        self.assert_successful_registration()
    
    def test_password_only_digits(self):
        self.selenium.get(self.url_login)
        self.fill_input_field("username", get_random_string())
        password = "123"
        self.fill_input_field("password", password)
        self.fill_input_field("password1", password)
        
        # the implementation won't make this test fail
        self.assert_successful_registration()
    
    def test_password_special_chars_and_boundary_char(self):
        self.selenium.get(self.url_login)
        self.fill_input_field("username", get_random_string())
        password = "ÆÅ~βpassword@£!£$:D]|>"
        self.fill_input_field("password", password)
        self.fill_input_field("password1", password)
        
        # the implementation won't make this test fail
        self.assert_successful_registration()
    
    def test_password_blank(self):
        self.selenium.get(self.url_login)
        
        username = get_random_string()
        blank_password = ""
        self.fill_input_field("username", username)
        self.fill_input_field("password", blank_password)
        self.fill_input_field("password1", blank_password)
        
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_xpath("//*[@id='btn-create-account']").click()
        time.sleep(TIME_WAIT)
        error = self.selenium.find_element_by_xpath("/html/body/div[1]").text
        self.assertIn("failed", error)
        self.assertIn("password", error)
        
    def test_password_length_1(self):
        self.selenium.get(self.url_login)
        
        username = get_random_string()
        password = "1"
        self.assertEqual(len(password), 1)
        self.fill_input_field("username", username)
        self.fill_input_field("password", password)
        self.fill_input_field("password1", password)

        # the implementation won't make this test fail, should have limit on lenght
        self.assert_successful_registration()
    
    def test_password_length_50(self):
        self.selenium.get(self.url_login)
        
        username = get_random_string()
        BOUNDARY = 50
        password = "a"*BOUNDARY
        self.assertEqual(len(password), BOUNDARY)
        
        self.fill_input_field("username", username)
        self.fill_input_field("password", password)
        self.fill_input_field("password1", password)
        
        # the implementation won't make this test fail, should have a limit on length
        self.assert_successful_registration()

    def test_email_without_at_symbol(self):
        self.selenium.get(self.url_login)
        self.fill_necessary_form()
        email = "email123.no"
        self.fill_input_field("email", email)
        
        self.assert_failed_registration()
        
        self.assert_invalid_email_field()
    
    def test_email_special_chars(self):
        self.selenium.get(self.url_login)
        self.fill_necessary_form()
        email = "[has]æ#~@secf1t.no"
        self.fill_input_field("email", email)
        
        self.assert_failed_registration()
        
        self.assert_invalid_email_field()
    
    def test_email_only_digits(self):
        self.selenium.get(self.url_login)
        self.fill_necessary_form()
        email = "1231456@secf1t.no"
        self.fill_input_field("email", email)
        
        # the implementation won't make this test fail
        self.assert_successful_registration()
    
    def test_phone_only_chars(self):
        self.selenium.get(self.url_login)
        self.fill_necessary_form()
        phone = "abcdefgh"
        self.fill_input_field("phone_number", phone)
        
        # the implementation won't make this test fail
        self.assert_successful_registration()
    
    def test_country_special_valid_symbols(self):
        self.selenium.get(self.url_login)
        self.fill_necessary_form()
        country = "Færøyene"
        self.fill_input_field("country", country)
        
        self.assert_successful_registration()
    
    def test_street_only_digits(self):
        self.selenium.get(self.url_login)
        self.fill_necessary_form()
        street = "234864"
        self.fill_input_field("street_address", street)
        
        # the implementation won't make this test fail
        self.assert_successful_registration()
        
@tag("integration") 
class TwoWayDomainTests(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = webdriver.Chrome("../../chromedriver.exe")
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


    url_register = "http://localhost:8080/register.html"
    url_workouts = "http://localhost:8080/workouts.html"
    
    email1 = "user@email.no"
    email2 = f"{get_random_string()}123"
    
    password1 = f"12{get_random_string()}!@:Dæøå"
    password2 = f"{get_random_string()}123"
    
    phone1 = "+4712345678"
    phone2 = "phonenumber"
    
    country1 = "Østerrike"
    country2 = "NORWAY123!"
    
    city1 = "Tromsø"
    city2 = "TROMSØ123"
    
    street1 = "Brøsetvegen 1234"
    street2 = "Pengevegen £"
        
    def fill_input_field(self, inputName, inputValue):
        field_input = self.selenium.find_element_by_name(inputName)
        field_input.send_keys(inputValue)
        
    def fill_in_register_form(self, username, email, password, phone, country, city, street):
        self.fill_input_field("username", username)
        self.fill_input_field("email", email)
        self.fill_input_field("password", password)
        self.fill_input_field("password1", password)
        self.fill_input_field("phone_number", phone)
        self.fill_input_field("country", country)
        self.fill_input_field("city", city)
        self.fill_input_field("street_address", street)
        time.sleep(TIME_WAIT)
    
    def assert_successful_registration(self):
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_id("btn-create-account").click()
        time.sleep(TIME_WAIT)
        expected_url = self.url_workouts
        redirected_url = self.selenium.current_url
        error_msg = "User was not redirected to main page"
        self.assertEqual(redirected_url, expected_url, error_msg)
        time.sleep(TIME_WAIT)
        
    
    def assert_failed_registration(self):
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_id("btn-create-account").click()
        time.sleep(TIME_WAIT)
        
        expected_url = self.url_register
        redirected_url = self.selenium.current_url
        error_msg = "User was redirected to another page"
        self.assertEqual(redirected_url, expected_url, error_msg)
        time.sleep(TIME_WAIT)
        
    def test_domain_case_1(self):
        username1 = f"{get_random_string()}ÆØÅ123!"
        username2 = get_random_string()
        
        self.selenium.get(self.url_register)
        
        self.fill_in_register_form(
            username=username1,
            email=self.email1,
            password=self.password1,
            phone=self.phone1,
            country=self.country1,
            city=self.city1,
            street=self.street1
        )
        
        self.assert_failed_registration()
        msg = self.selenium.find_element_by_xpath("/html/body/div[1]").text.lower()
        self.assertIn("failed", msg) 
        self.assertIn("username", msg)
        time.sleep(TIME_WAIT)
    
    def test_domain_case_2(self):
        username1 = f"{get_random_string()}ÆØÅ123!"
        username2 = get_random_string(random.randint(3,20))
        
        self.selenium.get(self.url_register)
        
        self.fill_in_register_form(
            username=username2,
            email=self.email2,
            password=self.password2,
            phone=self.phone2,
            country=self.country2,
            city=self.city2,
            street=self.street1
        )
        
        self.assert_failed_registration()
        msg = self.selenium.find_element_by_xpath("/html/body/div[1]").text.lower()
        self.assertIn("failed", msg) 
        self.assertIn("email", msg)
        time.sleep(TIME_WAIT)
    
    def test_domain_case_3(self):
        username1 = f"{get_random_string()}ÆØÅ123!"
        username2 = get_random_string()
        
        self.selenium.get(self.url_register)
        
        self.fill_in_register_form(
            username=username1,
            email=self.email2,
            password=self.password1,
            phone=self.phone2,
            country=self.country2,
            city=self.city2,
            street=self.street1
        )
        
        self.assert_failed_registration()
        msg = self.selenium.find_element_by_xpath("/html/body/div[1]").text.lower()
        self.assertIn("failed", msg) 
        self.assertIn("email", msg)
        self.assertIn("username", msg)
        time.sleep(TIME_WAIT)

    def test_domain_case_4(self):
        username1 = f"{get_random_string()}ÆØÅ123!"
        username2 = get_random_string(random.randint(3,20))
        
        self.selenium.get(self.url_register)
        
        self.fill_in_register_form(
            username=username2,
            email=self.email1,
            password=self.password2,
            phone=self.phone1,
            country=self.country1,
            city=self.city1,
            street=self.street2
        )
        
        self.assert_successful_registration()

    def test_domain_case_5(self):
        username1 = f"{get_random_string()}ÆØÅ123!"
        username2 = get_random_string()
        
        self.selenium.get(self.url_register)
        
        self.fill_in_register_form(
            username=username1,
            email=self.email2,
            password=self.password2,
            phone=self.phone1,
            country=self.country2,
            city=self.city1,
            street=self.street2
        )
        
        self.assert_failed_registration()
        msg = self.selenium.find_element_by_xpath("/html/body/div[1]").text.lower()
        self.assertIn("failed", msg)
        self.assertIn("username", msg) 
        self.assertIn("email", msg)
        time.sleep(TIME_WAIT)
    
    def test_domain_case_6(self):
        username1 = f"{get_random_string()}ÆØÅ123!"
        username2 = get_random_string()
        
        self.selenium.get(self.url_register)
        
        self.fill_in_register_form(
            username=username2,
            email=self.email1,
            password=self.password1,
            phone=self.phone2,
            country=self.country1,
            city=self.city1,
            street=self.street1
        )
        
        self.assert_successful_registration()
    
    def test_domain_case_7(self):
        username1 = f"{get_random_string()}ÆØÅ123!"
        username2 = get_random_string(random.randint(3,20))
        
        self.selenium.get(self.url_register)
        
        self.fill_in_register_form(
            username=username1,
            email=self.email1,
            password=self.password2,
            phone=self.phone2,
            country=self.country1,
            city=self.city2,
            street=self.street1
        )
        
        self.assert_failed_registration()
        msg = self.selenium.find_element_by_xpath("/html/body/div[1]").text.lower()
        self.assertIn("failed", msg) 
        self.assertIn("username", msg)
        time.sleep(TIME_WAIT)
    
    def test_domain_case_8(self):
        username1 = f"{get_random_string()}ÆØÅ123!"
        username2 = get_random_string()
        
        self.selenium.get(self.url_register)
        
        self.fill_in_register_form(
            username=username2,
            email=self.email1,
            password=self.password1,
            phone=self.phone1,
            country=self.country2,
            city=self.city1,
            street=self.street2
        )
        
        self.assert_successful_registration()
    
