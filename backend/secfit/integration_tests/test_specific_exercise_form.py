import random
import string
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.test import tag
import time

TIME_WAIT = 1

def get_random_string(length=random.randint(3, 20)):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for _ in range(length))

@tag("integration")
class ExerciseFormBoundaryTests(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = webdriver.Chrome("../../chromedriver.exe")
        
    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
        
    url_login = "http://localhost:8080/login.html"
    url_exercises = "http://localhost:8080/exercises.html"
    url_exercise = "http://localhost:8080/exercise.html"
        
    def login(self):
        self.selenium.get(self.url_login)
        
        self.fill_input_field("username", "admin")
        self.fill_input_field("password", "123")
        self.selenium.find_element_by_xpath("//*[@id='btn-login']").click()
        time.sleep(TIME_WAIT)

    def fill_input_field(self, inputName, inputValue):
        field_input = self.selenium.find_element_by_name(inputName)
        field_input.send_keys(inputValue)

    def assert_successful_submission(self):
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_xpath("//*[@id='btn-ok-exercise']").click()
        time.sleep(TIME_WAIT)

        expected_url = self.url_exercises
        redirected_url = self.selenium.current_url
        self.assertEqual(redirected_url, expected_url, "User was not redirected to exercises.html")
        
        
    def assert_failed_submission(self):
        self.selenium.find_element_by_id("btn-ok-exercise").click()
        time.sleep(TIME_WAIT)
        
        expected_url = self.url_exercise
        redirected_url = self.selenium.current_url
        self.assertEqual(redirected_url, expected_url, "User was redirected to another page")
    
    def fill_default_specific_exercise_form(self):
        self.fill_input_field("name", f"Exercise {get_random_string()}")
        self.fill_input_field("description", "Description")
        self.fill_input_field("unit", "unit")
        self.fill_input_field("duration", 3600)
        self.fill_input_field("calories", 1000)
        time.sleep(TIME_WAIT)
        
    def fill_specific_exercise_form(self):
        self.selenium.find_element_by_xpath("//*[@id='inputExerciseType']/option[2]").click()
        self.fill_input_field("distance", 1000)
        self.fill_input_field("lapTime", 25)
        self.fill_input_field("avgSpeed", 10)
        self.fill_input_field("pulseRate", 120)
        time.sleep(TIME_WAIT)
    
    def test_create_swimming_exercise(self):
        self.login()
        self.selenium.get(self.url_exercise)
        self.fill_default_specific_exercise_form()
        self.fill_specific_exercise_form()
        self.selenium.find_element_by_xpath("//*[@id='inputSportType']/option[2]").click()
        time.sleep(TIME_WAIT)
        
        self.assert_successful_submission()
    
    def test_create_cycling_exercise(self):
        self.login()
        self.selenium.get(self.url_exercise)
        self.fill_default_specific_exercise_form()
        self.fill_specific_exercise_form()
        self.selenium.find_element_by_xpath("//*[@id='inputSportType']/option[3]").click()
        time.sleep(TIME_WAIT)
        
        self.assert_successful_submission()
    
    def test_create_running_exercise(self):
        self.login()
        self.selenium.get(self.url_exercise)
        self.fill_default_specific_exercise_form()
        self.fill_specific_exercise_form()
        self.selenium.find_element_by_xpath("//*[@id='inputSportType']/option[4]").click()
        time.sleep(TIME_WAIT)
    
        self.assert_successful_submission()
    
    def test_create_other_type_exercise(self):
        self.login()
        self.selenium.get(self.url_exercise)
        self.fill_default_specific_exercise_form()
        self.fill_specific_exercise_form()
        self.selenium.find_element_by_xpath("//*[@id='inputSportType']/option[1]").click()
        time.sleep(TIME_WAIT)
        
        self.assert_successful_submission()
    
    def test_edit_exercise(self):
        self.login()
        self.selenium.find_element_by_xpath("//*[@id='nav-exercises']").click()
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_xpath("//*[@id='div-content']/a[1]").click()
        time.sleep(TIME_WAIT)

        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_id("btn-edit-exercise").click()
        
        # check that exerciseType-field is disabled        
        exerciseType = self.selenium.find_element_by_name("exerciseType")
        self.assertFalse(exerciseType.is_enabled())
        
        # edit an arbitrary field
        self.fill_input_field("duration", 123)
        self.selenium.find_element_by_xpath("//*[@id='btn-ok-exercise']").click()
        
        time.sleep(TIME_WAIT)
        self.assertEqual(self.selenium.current_url, self.selenium.current_url, "User was redirected")
        time.sleep(TIME_WAIT)
    
    def test_delete_exercise(self):
        self.login()
        self.selenium.find_element_by_xpath("//*[@id='nav-exercises']").click()
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_xpath("//*[@id='div-content']/a[1]").click()
        time.sleep(TIME_WAIT)
        
        html = self.selenium.find_element_by_tag_name('html')
        html.send_keys(Keys.END)
        time.sleep(TIME_WAIT)
        self.selenium.find_element_by_xpath("//*[@id='btn-edit-exercise']").click()
        
        exerciseType = self.selenium.find_element_by_name("exerciseType")
        self.assertFalse(exerciseType.is_enabled())
        
        self.selenium.find_element_by_xpath("//*[@id='btn-delete-exercise']").click()
        
        time.sleep(TIME_WAIT)
        expected_url = self.url_exercises
        self.assertEqual(self.selenium.current_url, expected_url, "User was not redirected")
        time.sleep(TIME_WAIT)
