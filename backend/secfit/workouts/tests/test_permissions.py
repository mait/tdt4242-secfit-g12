from django.urls import reverse
from django.test import TestCase
from workouts.permissions import IsOwnerOfWorkout, IsCoachAndVisibleToCoach, IsCoachOfWorkoutAndVisibleToCoach, IsReadOnly, IsPublic, IsWorkoutPublic
from workouts.models import Workout, Exercise, ExerciseInstance
from workouts.permissions import IsOwner
from django.test.client import RequestFactory
from django.contrib.auth import get_user_model

class WorkoutPermissionTests(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(username="user")
        self.owner = get_user_model().objects.create(username="owner")
        self.factory = RequestFactory()
        
        self.url = reverse("workout-list")
        
        self.workout = Workout.objects.create(
            name="Workout",
            date="2022-02-22T12:22:22Z",
            notes="notes",
            owner=self.owner,
            visibility="PU"
        )
        
        exercise = Exercise.objects.create(name="exercise", description="description", unit="unit")
        self.exerciseInstance = ExerciseInstance.objects.create(
            workout=self.workout,
            exercise=exercise,
            sets=10,
            number=10
        )
        
    def test_is_owner(self):
        requestUser = self.factory.get(self.url)
        requestOwner = self.factory.get(self.url)
        requestUser.user = self.user
        requestOwner.user = self.owner
        
        isOwner = IsOwner()
        VALID_PERMISSION = isOwner.has_object_permission(requestOwner, None, self.workout)
        self.assertTrue(VALID_PERMISSION)
        
        NOT_VALID_PERMISSION = isOwner.has_object_permission(requestUser, None, self.workout)
        self.assertFalse(NOT_VALID_PERMISSION)
        
    def test_is_owner_of_workout(self):
        requestOwner = self.factory.post(self.url)
        requestOwner.user = self.owner
        
        requestUser = self.factory.post(self.url)
        requestUser.user = self.user

        data = {
            "workout": f"{self.url}{self.workout.id}/"
        }
        requestOwner.data = data
        
        isOwnerOfWorkout = IsOwnerOfWorkout()
        VALID_POST_PERMISSION = isOwnerOfWorkout.has_permission(requestOwner, None)
        self.assertTrue(VALID_POST_PERMISSION)
        
        requestUser.data = { "invalid": "data" }
        INVALID_POST_PERMISSION = isOwnerOfWorkout.has_permission(requestUser, None)
        self.assertFalse(INVALID_POST_PERMISSION)
    
        requestOwner = self.factory.get(self.url)
        requestOwner.user = self.owner
        requestOwner.data = data
        
        VALID_GET_PERMISSION = isOwnerOfWorkout.has_permission(requestOwner, None)
        self.assertTrue(VALID_GET_PERMISSION)
        
        VALID_OBJ_PERMISSION = isOwnerOfWorkout.has_object_permission(requestOwner, None, self.exerciseInstance)
        self.assertTrue(VALID_OBJ_PERMISSION)
    
    def test_is_coach_and_visible_to_coach(self):
        permission = IsCoachAndVisibleToCoach()
        request = self.factory.get(self.url)
        request.user = self.owner
        self.owner.coach = self.owner
        
        VALID_OBJ_PERMISSION = permission.has_object_permission(request, None, self.workout)
        self.assertTrue(VALID_OBJ_PERMISSION)
    
    def test_is_coach_of_workout_and_visible_to_coach(self):        
        request = self.factory.get(self.url)
        request.user = self.owner
        self.owner.coach = self.owner
        
        permission = IsCoachOfWorkoutAndVisibleToCoach()
        VALID_OBJ_PERMISSION = permission.has_object_permission(request, None, self.exerciseInstance)
        self.assertTrue(VALID_OBJ_PERMISSION)
    
    def test_is_public(self):
        request = self.factory.get(self.url)
        permission = IsPublic()
        isPublic = permission.has_object_permission(request, None, self.workout)
        self.assertTrue(isPublic)
        
    def test_is_workout_public(self):        
        request = self.factory.get(self.url)
        permission = IsWorkoutPublic()
        isWorkoutPublic = permission.has_object_permission(request, None, self.exerciseInstance)
        self.assertTrue(isWorkoutPublic)
        
    
    def test_is_read_only(self):
        request = self.factory.get(self.url)
        permission = IsReadOnly()
        isReadOnly = permission.has_object_permission(request, None, self.workout)
        self.assertTrue(isReadOnly)
    