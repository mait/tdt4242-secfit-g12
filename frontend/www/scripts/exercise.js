let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;

class MuscleGroup { 
    constructor(type) {
        this.isValidType = false;
        this.validTypes = ["Legs", "Chest", "Back", "Arms", "Abdomen", "Shoulders"]

        this.type = this.validTypes.includes(type) ? type : undefined;
    }

    setMuscleGroupType = (newType) => {
        this.isValidType = false;
        
        if(this.validTypes.includes(newType)){
            this.isValidType = true;
            this.type = newType;
        }
        else{
            alert("Invalid muscle group!");
        }

    };
    
    getMuscleGroupType = () => {
        console.log(this.type, "SWIOEFIWEUFH")
        return this.type;
    }
}

function handleSetFormReadOnly(isSpecificType) {
    setReadOnly(true, "#form-exercise");
    document.querySelector("select").setAttribute("disabled", "")
    okButton.className += " hide";
    deleteButton.className += " hide";
    cancelButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);

    oldFormData.delete("name");
    oldFormData.delete("description");
    oldFormData.delete("duration");
    oldFormData.delete("calories");
    oldFormData.delete("muscleGroup");
    oldFormData.delete("unit");

    if (isSpecificType) {
        oldFormData.delete("sportType");
        oldFormData.delete("distance");
        oldFormData.delete("lapTime");
        oldFormData.delete("avgSpeed");
        oldFormData.delete("pulseRate");
    }

}

function handleCancelButtonDuringEdit() {
    const urlParams = new URLSearchParams(window.location.search);
    const isSpecificType = Boolean(urlParams.get("isSpecificExercise"));

    
    let form = document.querySelector("#form-exercise");
    if (oldFormData.has("name")) form.name.value = oldFormData.get("name");
    if (oldFormData.has("description")) form.description.value = oldFormData.get("description");
    if (oldFormData.has("duration")) form.duration.value = oldFormData.get("duration");
    if (oldFormData.has("calories")) form.calories.value = oldFormData.get("calories");
    if (oldFormData.has("muscleGroup")) form.muscleGroup.value = oldFormData.get("muscleGroup");
    if (oldFormData.has("unit")) form.unit.value = oldFormData.get("unit");
    
    if (isSpecificType) {
        if (oldFormData.has("sportType")) form.sportType.value = oldFormData.get("sportType");
        if (oldFormData.has("distance")) form.distance.value = oldFormData.get("distance");
        if (oldFormData.has("lapTime")) form.lapTime.value = oldFormData.get("lapTime");
        if (oldFormData.has("avgSpeed")) form.avgSpeed.value = oldFormData.get("avgSpeed");
        if (oldFormData.has("pulseRate")) form.pulseRate.value = oldFormData.get("pulseRate");
    }

    handleSetFormReadOnly(isSpecificType);

}


async function createExercise() {
    document.querySelector("select").removeAttribute("disabled")
    let form = document.querySelector("#form-exercise");
    let formData = new FormData(form);
    
    const exerciseType = formData.get("exerciseType");
    let body, url;    

    if (exerciseType.toLowerCase() === "strength endurance") {
        body = {
                "name": formData.get("name"), 
                "description": formData.get("description"),
                "duration": formData.get("duration"),
                "calories": formData.get("calories"),
                "muscleGroup": formData.get("muscleGroup"), 
                "unit": formData.get("unit")
            };
        url = `${HOST}/api/exercises/`;
    } else {
        body = {
            "name": formData.get("name"), 
            "description": formData.get("description"),
            "duration": formData.get("duration"),
            "calories": formData.get("calories"),
            "muscleGroup": formData.get("muscleGroup"), 
            "unit": formData.get("unit"),
            "sportType": formData.get("sportType"),
            "distance": formData.get("distance"),
            "lapTime": formData.get("lapTime"),
            "avgSpeed": formData.get("avgSpeed"),
            "pulseRate": formData.get("pulseRate")
        }
        url = `${HOST}/api/specific-exercises/`
    }

    let response = await sendRequest("POST", url, body);

    if (response.ok) {
        window.location.replace("exercises.html");
    } else {
        let data = await response.json();
        let alert = createAlert("Could not create new exercise!", data);
        document.body.prepend(alert);
    }
}

function handleEditExerciseButtonClick() {
    setReadOnly(false, "#form-exercise");

    document.querySelector("select").removeAttribute("disabled")
    document.getElementById("inputExerciseType").setAttribute("disabled", "true");
    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");
    deleteButton.className = deleteButton.className.replace(" hide", "");

    cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-exercise");
    oldFormData = new FormData(form);
}

async function deleteExercise(id, isSpecificType) {
    let url;

    isSpecificType
    ? url = `${HOST}/api/specific-exercises/${id}/`
    : url = `${HOST}/api/exercises/${id}/`;

    let response = await sendRequest("DELETE", url);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not delete exercise ${id}`, data);
        document.body.prepend(alert);
    } else {
        window.location.replace("exercises.html");
    }
}

async function retrieveExercise(id, isSpecificType) {
    let sportType = document.querySelector("#sportType");
    let distance = document.querySelector("#distance");
    let lapTime = document.querySelector("#lapTime");
    let avgSpeed = document.querySelector("#avgSpeed");
    let pulseRate = document.querySelector("#pulseRate");
    let response;

    isSpecificType
    ? response =  await sendRequest("GET", `${HOST}/api/specific-exercises/${id}`)
    : response = await sendRequest("GET", `${HOST}/api/exercises/${id}/`)

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve exercise data!", data);
        document.body.prepend(alert);
    } else {
        document.querySelector("select").removeAttribute("disabled")
        let exerciseData = await response.json();
        let form = document.querySelector("#form-exercise");
        let formData = new FormData(form);
        if (isSpecificType) {
            console.log(isSpecificType)
            sportType.style.display = "unset";
            distance.style.display = "unset";
            lapTime.style.display = "unset";
            avgSpeed.style.display = "unset";
            pulseRate.style.display = "unset";
        } else {
            sportType.style.display = "none";
            distance.style.display = "none";
            lapTime.style.display = "none"; 
            avgSpeed.style.display = "none";
            pulseRate.style.display = "none"; 
        }

        for (let key of formData.keys()) {
            let selector;
            key !== "muscleGroup"? selector = `input[name="${key}"], textarea[name="${key}"]` : selector = `select[name=${key}]`
            
            let input = form.querySelector(selector);
            let newVal = exerciseData[key];
            if (newVal === undefined) { continue; }
            
            input.value = newVal;
        }

        let muscleGroupInput = form.querySelector("select[name=muscleGroup]");
        // bad use of variable name
        let muscleVal = exerciseData["muscleGroup"];
        muscleGroupInput.value = muscleVal;
        
        let exerciseTypeInput = form.querySelector("select[name=exerciseType]");
        if (isSpecificType) {
            let sportTypeInput = form.querySelector("select[name=sportType]");
            let sportTypeVal = exerciseData["sportType"];
            sportTypeInput.value = sportTypeVal;

            exerciseTypeInput.value = "Specific sport exercise";
        } else {
            exerciseTypeInput.value = "Strength endurance"
        }
        document.querySelector("select").setAttribute("disabled", "")
    }
}

async function updateExercise(id, isSpecificType) {
    let form = document.querySelector("#form-exercise");
    let formData = new FormData(form);

    let muscleGroupSelector = document.querySelector("#muscleGroup");
    muscleGroupSelector.removeAttribute("disabled");
    
    
    let selectedMuscleGroup = new MuscleGroup(formData.get("muscleGroup"));
    let body, url;
    
    if (isSpecificType) {
        url = `${HOST}/api/specific-exercises/${id}/`;
        body = {
            "name": formData.get("name"), 
            "description": formData.get("description"),
            "duration": formData.get("duration"),
            "calories": formData.get("calories"),
            "muscleGroup": selectedMuscleGroup.getMuscleGroupType(),
            "unit": formData.get("unit"),
            "sportType": formData.get("sportType"),
            "distance": formData.get("distance"),
            "lapTime": formData.get("lapTime"),
            "avgSpeed": formData.get("avgSpeed"),
            "pulseRate": formData.get("pulseRate")
        }
    }else {
        url = `${HOST}/api/exercises/${id}/`;
        body = {
            "name": formData.get("name"), 
            "description": formData.get("description"),
            "duration": formData.get("duration"),
            "calories": formData.get("calories"),
            "muscleGroup": selectedMuscleGroup.getMuscleGroupType(),
            "unit": formData.get("unit")
        };
    }

    let response = await sendRequest("PUT", url, body);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not update exercise ${id}`, data);
        document.body.prepend(alert);
    } else {
        muscleGroupSelector.setAttribute("disabled", "")
        handleSetFormReadOnly(isSpecificType)
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    cancelButton = document.querySelector("#btn-cancel-exercise");
    okButton = document.querySelector("#btn-ok-exercise");
    deleteButton = document.querySelector("#btn-delete-exercise");
    editButton = document.querySelector("#btn-edit-exercise");
    oldFormData = null;


    const urlParams = new URLSearchParams(window.location.search);
    // view/edit
    if (urlParams.has('id')) {
        const exerciseId = urlParams.get('id');
        const isSpecificType = Boolean(urlParams.get("isSpecificExercise"));

        await retrieveExercise(exerciseId, isSpecificType);

        editButton.addEventListener("click", handleEditExerciseButtonClick);
        deleteButton.addEventListener("click", (async (id) => await deleteExercise(id, isSpecificType)).bind(undefined, exerciseId));
        okButton.addEventListener("click", (async (id) => await updateExercise(id, isSpecificType)).bind(undefined, exerciseId));
    } 
    //create
    else {
        setReadOnly(false, "#form-exercise");

        editButton.className += " hide";
        okButton.className = okButton.className.replace(" hide", "");
        cancelButton.className = cancelButton.className.replace(" hide", "");

        okButton.addEventListener("click", async () => await createExercise());
        cancelButton.addEventListener("click", () => window.location.replace("exercises.html"));
    }
});