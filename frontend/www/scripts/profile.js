async function getProfileFacts() {
    let user = null;
    user = await getCurrentUser();

    if (user != null) {
        let username = document.querySelector("#profile-username");
        username.textContent = `${user.username}`;


        let table = document.querySelector("table");
        let rows = table.querySelectorAll("tr");

        rows[0].querySelectorAll("td")[1].textContent = user.email; // Email
        rows[1].querySelectorAll("td")[1].textContent = user.phone_number; // Phonenumber
        rows[2].querySelectorAll("td")[1].textContent = user.country; //Country
        rows[3].querySelectorAll("td")[1].textContent = user.city; //City
        rows[4].querySelectorAll("td")[1].textContent = user.street_address; //Street address
        let coach = null;
        if (user.coach){
            let response = await sendRequest("GET", user.coach);
            let coach_response = await response.json(); 
            coach = coach_response.username;
        }
        rows[5].querySelectorAll("td")[1].textContent = coach || "No coach registered";

        let atletes = "";
        for (let athleteUrl of user.athletes){
            let response = await sendRequest("GET", athleteUrl);
            let athlete = await response.json();
            atletes += athlete.username + "\n";
        }

        let athlete_list = document.querySelector("#profile-athletes");
        athlete_list.textContent = atletes || 'You have no athletes';


        let container = document.getElementById('div-content');
        for (let workoutUrl of user.workouts) {
            let response = await sendRequest("GET", workoutUrl);
            let workout = await response.json();
            let templateWorkout = document.querySelector("#template-workout");
            let cloneWorkout = templateWorkout.content.cloneNode(true);

            let aWorkout = cloneWorkout.querySelector("a");
            aWorkout.href = `workout.html?id=${workout.id}`;

            let h5 = aWorkout.querySelector("h5");
            h5.textContent = workout.name;

            let localDate = new Date(workout.date);

            let table = aWorkout.querySelector("table");
            let rows = table.querySelectorAll("tr");
            rows[0].querySelectorAll("td")[1].textContent = localDate.toLocaleDateString(); // Date
            rows[1].querySelectorAll("td")[1].textContent = localDate.toLocaleTimeString(); // Time
            rows[2].querySelectorAll("td")[1].textContent = workout.owner_username; //Owner
            rows[3].querySelectorAll("td")[1].textContent = workout.exercise_instances.length; // Exercises

            container.appendChild(aWorkout);
        };

    } else {
        console.log("NO USER FOUND")
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    await getProfileFacts();
});