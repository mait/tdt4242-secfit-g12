let user = null;

async function saveChanges(id){
    let form = document.querySelector("#form-edit-user");
    let formData = new FormData(form);
    let url = `${HOST}/api/users/${id}/`;
    let body = {
        "email": formData.get("email"),
        "username":       user.username,
        "phone_number":   formData.get("phone_number"),
        "country":        formData.get("country"),
        "city":           formData.get("city"),
        "street_address": formData.get("street_address"),
    }

    
    console.log(body)
    let response = await sendRequest("PUT", url, body);

    if (response.ok) {
        window.location.replace("profile.html");
    } else {
        let data = await response.json();
        let alert = createAlert("Could not save changes!", data);
        document.body.prepend(alert);
    }
}

async function retrieveInfo() {
    user = await getCurrentUser();
    let form = document.querySelector("#form-edit-user");
    let formData = new FormData(form);
    

    for (let key of formData.keys()) {
        let selector = `input[name="${key}"], textarea[name="${key}"]`;
        let input = form.querySelector(selector);
        let newVal = user[key];
        if (key != "password" && key != "password1") {
            input.value = newVal;
        }
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    await retrieveInfo();
    let saveChangesBtn = document.querySelector("#btn-save-changes");
    saveChangesBtn.addEventListener("click", async () => await saveChanges(user.id));
});